const fonts = {}

const sanitizeHtml = html => {
  return html.replace(/\n/g, '').replace(/\ {2,}/g, ' ').replace(/href=/g, 'target="_blank" rel="noopener noreferrer" href=').trim()
}

const font = (date, data) => {
  data.date = date
  data.about = sanitizeHtml(data.about)
  if (data.fontFace) {
    data.fontFace = sanitizeHtml(data.fontFace)
  }
  data.designers = data.designers.map((d, i) => {
    if (d.bio) d.bio = sanitizeHtml(d.bio)
    return d
  })
  fonts[date] = data
}

font(1, {
  name: 'Graduate',
  provider: 'google',
  css: `font-family: 'Graduate', sans-serif;`,
  styles: '400',
  subtitle: 'On the first day of Christmas my true love gave to me college block style lettering in a pear tree.',
  sample: 'O Christmas tree, O Christmas tree, how are thy leaves so verdant!',
  author: {
    image: 'lucas-cz.jpg',
    name: 'Lucas Czarnecki',
    title: 'Managing Editor',
    company: 'TYPE Magazine',
    quote: `This slab serif desinged by Eduardo Tunni pays homage to traditional varsity lettering and boasts a wide variety of variable axes. From optical size to serifs, from "y-opaque" to "x-transparent," Graduate gives web typographers fine control over the appearance of their design. I'd recommend using this anytime you need distinctive, impactful display type.`,
    website: 'https://lucasczarnecki.com/'
  },
  designers: [
    {
      image: null,
      name: 'Eduardo Tunni',
      bio: null
    }
  ],
  about: `<p>Graduate is a high quality example of the classic college block style of lettering used across every campus in the USA.</p> <p>To contribute to the project contact <a href="mailto:edu@tipo.net.ar">Eduardo Tunni</a>.</p>`,
  chars: `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(2, {
  name: 'Source Sans Pro',
  provider: 'google',
  css: `font-family: 'Source Sans Pro', sans-serif;`,
  styles: '200,200i,300,300i,400,400i,600,600i,700,700i,900,900i',
  subtitle: 'On the second day of Christmas my true love gave to me a versatile gothic inspired sans serif.',
  sample: 'There must have been some magic in that old silk hat they found.',
  author: {
    image: 'asher-blumberg.jpg',
    name: 'Asher Blumberg',
    title: 'Product Designer',
    company: 'Task Rabbit',
    quote: `I love this font because of it’s versatility, coming in at six unique weights (diversity plug). Source Sans pro is Adobe's first open-source font. It’s classy like Franklin Gothic but new and modern at the same time. Now that’s meta!`,
    website: 'http://asherblumberg.com/?fbclid=IwAR2P3k5pSkwESp7LMbe54FZA9yezrdrc5HWfn6dIjMhmgxFSKvIccZr1N90#taskrabbit'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/pXkkq9v6LZQyR5y1YSr2qtOkZ3kHVgutUz4PqXvWzH_Etqe53Xf8PPs0JN0',
      name: 'Paul D. Hunt',
      bio: null
    }
  ],
  about: `<p>Source Sans Pro, Adobe's first open source typeface family, was designed by Paul D. Hunt. It is a sans serif typeface intended to work well in user interfaces.</p>`,
  chars: `ABCČDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžЁЄЖЗЅИІЇЙЯёєћуўфхΑΒΓΔΕΖΗΘΦΧΨΩαβγδ1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(3, {
  name: 'IBM Plex Mono',
  provider: 'google',
  css: `font-family: 'IBM Plex Mono', monospace;`,
  styles: '100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i',
  subtitle: 'On the third day of Christmas my true love gave to me a friendly grotesque in the midst of mankind and machine.',
  sample: 'Oh, bring us some figgy pudding and a cup of good cheer.',
  author: {
    image: 'neil-shankar.jpg',
    name: 'Neil Shankar',
    title: 'Designer',
    company: 'Leftfield Labs',
    quote: `Plex is full of beauty and utility. It’s technical, precise, and comes in many weights. I wish it were my system default.`,
    website: 'https://www.tallneil.io/'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/fjNcs1H04z_dEPy32d-c16-XBKZhciEEl0cCUX_x-ZOwEeLj0Up8XkpXxOU',
      name: 'Mike Abbink',
      bio: `
        <p>Mike is the Executive Creative Director at IBM Brand Experience & Design. Mike has an extensive career as a type designer, creating successful typeface families, such as FF Kievit, FF Milo and Brando.</p>
        <p><a href="https://www.mikeabbink.com">Homepage</a> | <a href="https://twitter.com/mikeabbink/">Twitter</a></p>
      `
    },
    {
      image: 'https://lh3.googleusercontent.com/6_AvVAL1nPs7SbVQSCzp2N02cwUgrvgz97kiWyqwtEr39FicEUOLEdriv50_',
      name: 'Bold Monday',
      bio: `
      <p>
        Bold Monday is an independent Dutch type foundry established in 2008 by Paul van der Laan and Pieter van Rosmalen.
        The Bold Monday library encompasses custom typeface design for high-profile, international clients, as well as state-of-the-art retail fonts for discerning designers.
      </p>
      <p>
        <a href="https://boldmonday.com">Homepage</a> |
        <a href="https://twitter.com/boldmonday/">Twitter</a>
      </p>
      `
    }
  ],
  about: `
    <p>IBM Plex™ is an international typeface family designed by Mike Abbink, IBM BX&D, in collaboration with Bold Monday, an independent Dutch type foundry.
      Plex was designed to capture IBM’s spirit and history, and to illustrate the unique relationship between mankind and machine—a principal theme for IBM since the turn of the century.
      The result is a neutral, yet friendly Grotesque style typeface that includes a
      <a href="https://fonts.google.com/specimen/IBM+Plex+Sans">Sans</a>,
      <a href="https://fonts.google.com/specimen/IBM+Plex+Sans+Condensed">Sans Condensed</a>,
      Mono, and
      <a href="https://fonts.google.com/specimen/IBM+Plex+Serif">Serif</a>
      and has excellent legibility in print, web and mobile interfaces.
      Plex’s three designs work well independently, and even better together.
      Use the Sans as a contemporary compadre, the Serif for editorial storytelling, or the Mono to show code snippets.
      The unexpectedly expressive nature of the italics give you even more options for your designs.
    </p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžĂÂÊÔƠƯă1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(4, {
  name: 'Gangster Grotesk',
  provider: 'https://www.freshfonts.io/',
  fontFace: `
    @font-face {
      font-family: 'Gangster Grotesk';
      src: url(/fonts/source/gangstergrotesk-light.otf);
      font-weight: 300;
      font-display: swap;
    }
    @font-face {
      font-family: 'Gangster Grotesk';
      src: url(/fonts/source/gangstergrotesk-regular.otf);
      font-weight: 400;
      font-display: swap;
    }
    @font-face {
      font-family: 'Gangster Grotesk';
      src: url(/fonts/source/gangstergrotesk-bold.otf);
      font-weight: 600;
      font-display: swap;
    }
  `,
  css: `font-family: 'Gangster Grotesk', sans-serif;`,
  styles: '300,400,600',
  subtitle: `On the fourth day of Christmas my true love gave to me a multifaceted typeface to celebrate of the lil' gangster in all of us.`,
  sample: 'Giddy-up jingle horse, pick up your feet, jingle around the clock.',
  author: {
    image: 'keene.jpg',
    name: 'Keene Niemack',
    title: 'Design Lead',
    company: 'Ueno SF',
    quote: `Gangster Grotesk brings all of the beautiful characteristics of a traditional grotesque cut while also delivering distinctive and funky moments through exaggerated terminals, condensed letterforms, and a gorgeous uppercase set.`,
    website: 'https://www.linkedin.com/in/keene-niemack-222078180/'
  },
  designers: [
    {
      image: null,
      name: 'Adrien Midzic',
      bio: `
        <p>The typeface was designed by Adrien Midzic with <a href="https://typefaces.pizza/">Pizza Typefaces</a></p>
        <p>Adrien is a french type designer working for S M L XL XXL brands and companies. Adrien imagines, draws and produces original lettering for logo operations.</p>
      `
    }
  ],
  about: `
    <p>A typeface that celebrates the beauty of contrast in typography and in people . Nodding to its historical roots in the 1920s, Gangster Grotesk is a contemporary typeface that combines a sharp contrast with angled terminal strokes that curve inward ever so slightly. These elements are simple but effective, lending the typeface character while being practically invisible at small sizes.</p>
    <p>A brave alternative to homogenized grotesks, it’s a multifunctional workhorse designed to do wonders both in print and on screen, including for logo design, brand identities, websites, packaging, posters and headlines. Its slightly condensed width is carefully tuned for small sized body text as well.</p>
  `,
  chars: `AÄÁÂÀBCDÐEÈFGHIÎJKLMNOÒÓÔÕÖPQRSŞTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890‰¹³²Ž‘?’“!”(%)[#]{@}/&\<-+÷×=>»«ØÆ&№₽₿¢¤$€ƒ₺₩↑↗→↘↓↙↔◊@tæ•º°±®©$€£¥¢:;,.*`
})

font(5, {
  name: 'Inter',
  provider: 'https://rsms.me/inter',
  fontFace: `
    @font-face {
      font-family: 'Inter';
      font-style:  normal;
      font-weight: 100;
      font-display: swap;
      src: url(/fonts/source/Inter-Thin.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-Thin.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  italic;
      font-weight: 100;
      font-display: swap;
      src: url(/fonts/source/Inter-ThinItalic.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-ThinItalic.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  normal;
      font-weight: 200;
      font-display: swap;
      src: url(/fonts/source/Inter-ExtraLight.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-ExtraLight.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  italic;
      font-weight: 200;
      font-display: swap;
      src: url(/fonts/source/Inter-ExtraLightItalic.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-ExtraLightItalic.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  normal;
      font-weight: 300;
      font-display: swap;
      src: url(/fonts/source/Inter-Light.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-Light.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  italic;
      font-weight: 300;
      font-display: swap;
      src: url(/fonts/source/Inter-LightItalic.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-LightItalic.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  normal;
      font-weight: 400;
      font-display: swap;
      src: url(/fonts/source/Inter-Regular.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-Regular.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  italic;
      font-weight: 400;
      font-display: swap;
      src: url(/fonts/source/Inter-Italic.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-Italic.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  normal;
      font-weight: 500;
      font-display: swap;
      src: url(/fonts/source/Inter-Medium.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-Medium.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  italic;
      font-weight: 500;
      font-display: swap;
      src: url(/fonts/source/Inter-MediumItalic.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-MediumItalic.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  normal;
      font-weight: 600;
      font-display: swap;
      src: url(/fonts/source/Inter-SemiBold.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-SemiBold.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  italic;
      font-weight: 600;
      font-display: swap;
      src: url(/fonts/source/Inter-SemiBoldItalic.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-SemiBoldItalic.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  normal;
      font-weight: 700;
      font-display: swap;
      src: url(/fonts/source/Inter-Bold.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-Bold.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  italic;
      font-weight: 700;
      font-display: swap;
      src: url(/fonts/source/Inter-BoldItalic.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-BoldItalic.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  normal;
      font-weight: 800;
      font-display: swap;
      src: url(/fonts/source/Inter-ExtraBold.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-ExtraBold.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  italic;
      font-weight: 800;
      font-display: swap;
      src: url(/fonts/source/Inter-ExtraBoldItalic.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-ExtraBoldItalic.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  normal;
      font-weight: 900;
      font-display: swap;
      src: url(/fonts/source/Inter-Black.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-Black.woff?v=3.11) format("woff");
    }
    @font-face {
      font-family: 'Inter';
      font-style:  italic;
      font-weight: 900;
      font-display: swap;
      src: url(/fonts/source/Inter-BlackItalic.woff2?v=3.11) format("woff2"),
           url(/fonts/Inter-BlackItalic.woff?v=3.11) format("woff");
    }
  `,
  css: `font-family: 'Inter', sans-serif;`,
  styles: '100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i',
  subtitle: 'On the fifth day of Christmas my true love gave to me a magestically crafted specimen for digital screens.',
  sample: 'I want a hippopotamus to play with and enjoy.',
  author: {
    image: 'gabriel-valdivia.jpg',
    name: 'Gabriel Valdivia',
    title: 'Head of Design',
    company: 'Canopy',
    quote: `Inter is a sleek typeface designed specifically for legibility in screen interfaces. It's a good alternative to system fonts like Roboto and San Francisco that adds more personality to your brand while retaining legibility. Inter is extremely flexible with a wide variety of weights, glyphs, and features that have been carefully hand-crafted. One thing that makes Inter particularly unique is their use of "Dynamic Metrics", where you can provide the optical font size, and the tracking and leading is automatically calculated for you. I've used Inter in personal and professional projects and remain very impressed by its ability to navigate different tones of a brand—from serious to more playful—seamlessly.`,
    website: 'https://twitter.com/gabrielvaldivia'
  },
  designers: [
    {
      image: '/fonts/designers/rasmus-anderson.jpg',
      name: 'Rasmus Anderson',
      bio: `<p>Inter was designed by <a href="https://rsms.me/inter">RSMS</a></p>`
    }
  ],
  about: `
    <p>
      Inter is a typeface carefully crafted & designed for computer screens. Inter features a tall x-height to aid in readability of mixed-case and lower-case text.
      Inter started out in late 2016 as an experiment to build a perfectly pixel-fitting font at a specific small size (11px) to get the best of both sharpness and readability.
    </p>
    <p>After initial usability testing which revealed that it was very difficult to read longer text, enhancements were made by crafting glyphs and kerning in a way that made for more variation in the rhythm and smoother vertical and horizontal stems. As Inter was being developed, it was tested on an internal version of Figma—where the author of Inter works as a designer—and slowly improved upon based on experience and feedback.</p>
    <p>Inter is offered as both traditional constant font files, as well as a variable font which contains all styles in a much smaller file size. Additionally, a variable font is ...variable! You can mix and match weight and italic angle as you please, forming theoretically infinite variations.</p>
  `,
  chars: `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 ?!()[]{}&^%$#@~*`
})

font(6, {
  name: 'Lato',
  provider: 'google',
  css: `font-family: 'Lato', sans-serif;`,
  styles: '100,100i,300,300i,400,400i,700,700i,900,900i',
  subtitle: 'On the sixth day of Christmas my true love gave to me a semi-rounded expression of warmth.',
  sample: 'Here we are as in olden days, happy golden days of yore.',
  author: {
    image: 'coco.jpg',
    name: 'Coco Liu',
    title: 'Designer and Founder',
    company: 'Social Haus',
    quote: `Lato is my go-to font. It’s on both my personal and business websites. It's one of those timeless fonts where for some reason I never get tired of how it looks.`,
    website: 'https://www.coco-liu.com/?fbclid=IwAR0wMkcHcQ0U1QPmLBZVsy4K-rufHTGQC7CFntpFaiW2aMoaSgi-9kaR4JY'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/f5ucXmmiKY69fc4okmp8toLawfJWtei5uheN0DHR-ATL8Bo0Xs_D1NbREQ8',
      name: 'Łukasz Dziedzic',
      bio: `<p>Łukasz is a Warsaw-based designer. During Poland's first free elections in 1989, he joined Gazeta Wyborcza, the first independent daily newspaper, and soon found a home in the design department co-creating page layouts and his first typeface. In 2007, he created a three-style Latin and Cyrillic corporate family for empik, one of Poland’s largest retail networks. In 2010, he started the Lato project, to develop a high-quality open-source font family.</p>`
    }
  ],
  about: `
    <p>Lato is a sans serif typeface family started in the summer of 2010 by Warsaw-based designer Łukasz Dziedzic. In December 2010 the Lato family was published under the Open Font License by his foundry tyPoland, with support from Google.</p>
    <p>When working on Lato, Łukasz tried to create a typeface that would seem quite “transparent” when used in body text but would display some original traits when used in larger sizes. He used classical proportions to give the letterforms familiar harmony and elegance. He created a sleek sans serif look, even though it does not follow any current trend.</p>
    <p>The semi-rounded details of the letters give Lato a feeling of warmth, while the strong structure provides stability and seriousness. “Male and female, serious but friendly. With the feeling of the Summer,” says Łukasz.</p>
  `,
  chars: `ABCĆDEFGHIJKLMNOPQRSŠTUVWXYZŽabcćdefghijklmnopqrsštuvwxyzž1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(7, {
  name: 'Droid Serif',
  provider: 'https://www.fontsquirrel.com/fonts/droid-serif',
  fontFace: `
    @font-face {
      font-family: 'Droid Serif';
      src: url('/fonts/source/DroidSerif-Regular-webfont.woff') format('woff');
      font-weight: normal;
      font-style: normal;
    }
    @font-face {
      font-family: 'Droid Serif';
      src: url('/fonts/source/DroidSerif-Italic-webfont.woff') format('woff');
      font-weight: normal;
      font-style: italic;
    }
    @font-face {
      font-family: 'Droid Serif';
      src: url('/fonts/source/DroidSerif-Bold-webfont.woff') format('woff');
      font-weight: 700;
      font-style: normal;
    }
    @font-face {
      font-family: 'Droid Serif';
      src: url('/fonts/source/DroidSerif-BoldItalic-webfont.woff') format('woff');
      font-weight: 700;
      font-style: italic;
    }
  `,
  css: `font-family: 'Droid Serif', serif;`,
  styles: '400,400i,700,700i',
  subtitle: 'On the seventh day of Christmas my true love gave to me an easy reading contemporary serif.',
  sample: 'Bells on bob-tails ring, making spirits bright.',
  author: {
    image: 'jeremiah-shoaf.jpg',
    name: 'Jeremiah Shoaf',
    title: 'Art director, Founder and CEO',
    company: 'Typewolf',
    quote: `My pick would be for DM Sans and DM Serif Text. They pair together beautifully and both were designed by Colophon Foundry, one of my favorite type studios.`,
    website: 'https://www.typewolf.com'
  },
  designers: [
    {
      image: '/fonts/designers/steve-matteson.jpg',
      name: 'Steve Matteson',
      bio: `<p>Droid Serif is an open-source serif typeface designed by Steve Matteson for use in the Android platform.</p>`
    }
  ],
  about: `
    <p>
      Droid Serif Pro Regular is a contemporary serif typeface family designed for comfortable reading on screen.
      Droid Serif is slightly condensed to maximize the amount of text displayed on small screens.
      Vertical stress, sturdy serifs and open forms contribute to the readability of Droid Serif while its proportion and overall design complement its companion Droid Sans.
      The Droid fonts were designed by Steve Matteson, the Type Director at Ascender Corporation. Droid Serif Pro Regular includes Latin 1 and WGL character sets, along with Old Style Figures (requires an application that support advanced OpenType typographic features).
      NOTE: An OpenType-savvy application such as Adobe Creative Suite or Quark XPress is required to access the OpenType typographic features.
    </p>
  `,
})

font(8, {
  name: 'Rubik',
  provider: 'google',
  css: `font-family: 'Rubik', sans-serif;`,
  styles: '300,300i,400,400i,500,500i,700,700i,900,900i',
  subtitle: 'On the eighth day of Christmas my true love gave to me a slightly rounded sans serif.',
  sample: 'Mark ye well the song we sing; gladsome tidings now we bring.',
  author: {
    image: 'jonathan-hung.jpg',
    name: 'Jonathon Hung',
    title: 'Design Manager',
    company: `Fortune 10<br/>Strategic Design student at Parsons`,
    quote: `I'm wary to share this one (afraid to have it jump the shark), but in the spirit of generosity, Rubik is a beautiful font with rounded corners - every designer's favorite. It looks great at many sizes. I had the opportunity to use the font on a project for San Francisco's city government website  and got to see how pretty it is in usage (except for the capital J blech). Also it's named after the Rubik's cube!`,
    website: 'https://jonathanhung.squarespace.com/'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/0ElX_D6HkFF_pM0ZXd18GFLVSwbp-e4ESqCB4kFjGXTt5skMstUVowhPxZQ',
      name: 'Hubert and Fischer',
      bio: null
    },
    {
      image: 'https://lh3.googleusercontent.com/SPnKA3KwGvu8A3WFCFaNZ2T-iZwuPhD2m97aRAUv3m4ZvHXGUzm5YIMPkNM',
      name: 'Meir Sadan',
      bio: `
        <p>Meir is a designer specializing in type design, typography, interactive digital design, and web development. He serves as Creative Director at <a href="http://feelter.net/">feelternet</a>, and teaches at Bezalel Academy of Arts and Design, and Minshar School for Art.</p>
        <p><a href="http://meirsadan.com/">meirsadan.com</a> | <a href="http://github.com/meirsadan/">GitHub</a> | <a href="http://twitter.com/meirsadan/">Twitter</a></p>
      `
    },
    {
      image: 'https://lh3.googleusercontent.com/8_Ko9pfyjgJZcRYFkYxecsGSTs5ybw8025-aYOi_M2KX4-L0fAkRY0PFnUk',
      name: 'Cyreal',
      bio: null
    }
  ],
  about: `
    <p>Rubik is a sans serif font family with slightly rounded corners designed by Philipp Hubert and Sebastian Fischer at <a href="http://hubertfischer.com">Hubert & Fischer</a> as part of the <a href="https://www.chrome.com/cubelab">Chrome Cube Lab</a> project.</p>
    <p>Rubik is a 5 weight family with Roman and Italic styles, that accompanies <a href="https://fonts.google.com/specimen/Rubik+Mono+One">Rubik Mono One</a>, a monospaced variation of the Black roman design.</p>
    <p>Meir Sadan redesigned the Hebrew component in 2015. Alexei Vanyashin redesigned the Cyrillic component in 2016.</p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯאבגדהוזחטיכךלמםנןסעפףצץקרשת1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*₪`
})

font(9, {
  name: 'Luckiest Guy',
  provider: 'google',
  css: `font-family: 'Luckiest Guy', sans-serif;`,
  styles: '400',
  subtitle: 'On the ninth day of Christmas my true love gave to me an energetically poignant onomatopoeia.',
  sample: 'See the blazing Yule before us—Fa la la la la, la la la la. ',
  author: {
    image: 'arianna-azevedo.jpg',
    name: 'Arianna Azevedo',
    title: 'Designer',
    company: 'Autodesk',
    quote: `My boyfriend has a youtube channel and every once in awhile I'll make a thumbnail for his videos. His channel is focused on toys/games/comics so I'm always looking for something that has that "comic book" vibe but isn't comic sans. :) Luckiest Guy works well for this. It's fun, bold, a little chunky, perfect for a handful of words on a small thumbnail. I like that it's uneven and the "o" is smaller then the rest, it gives it some nice movement. Sometimes you just need a display font that makes the word "KABOOM" look cool.`,
    website: ''
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/dB25CHTk-wGoefHzSnQTtfWay6TwYiT3-uR11vgwkQamwVF6O-Ni61z8fSk',
      name: 'Astigmatic',
      bio: null
    }
  ],
  about: `<p>Luckiest Guy is a friendly heavyweight sans serif typeface inspired by 1950s advertisements with custom hand lettering.</p>`,
})

font(10, {
  name: 'Recursive Sans',
  demoStyle: 'Bold',
  provider: 'https://www.recursive.design/',
  fontFace: `
    @font-face {
      font-family: 'Recursive Sans';
      font-weight: 300 1000;
      src: url('/fonts/source/recursive-sans.woff2');
    }
  `,
  css: `font-family: 'Recursive Sans', sans-serif;`,
  styles: '300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i,1000,1000i',
  subtitle: 'On the tenth day of Christmas my true love gave to me a typographic palette for vibrant code & UI.',
  sample: `It's Christmas time in the city; ring-a-ling, hear them sing.`,
  author: {
    image: 'carly.jpg',
    name: 'Carly Ayres',
    title: 'Community & Culture',
    company: 'Google',
    quote: `Recursive first caught my interest when it was described as a "five-axis variable font." What does that mean? Designed by the good folks at Arrow Type, Recursive is a radically flexible typeface, adaptable by each of its axes: Proportion, Monospace, Weight, Slant, and Italic. It uses variable font technology to allow a wide range of tweaks and customizability -- all from a single font file -- making it ideal for those constantly shifting, scalable, digital environments. And it's beautiful. The type takes its inspiration from single-stroke brush sign painting.`,
    website: 'http://carlyayres.com'
  },
  designers: [
    {
      image: '/fonts/designers/arrow-type.png',
      name: 'Arrow Type',
      bio: `
        <p>With contributions from Lisa Huang, Katja Schimmel, Rafał Buchner.</p>
        <p>Type mastering with Ben Kiel</p>
        <p>Early guidance from KABK TypeMedia faculty</p>
      `
    }
  ],
  about: `
    <p>A highly-flexible new five-axis variable font, Recursive was built to maximize versatility, control, and performance. Taking full advantage of variable font technology, Recursive offers an unprecedented level of flexibility, all from a single font file. You can choose from a wide range of predefined styles, or dial in exactly what you want for each of its axes.</p>
    <p>Recursive draws inspiration from single-stroke casual signpainting, a style of brush writing that is stylistically flexible and warmly energetic. Adapting this aesthetic to excel in digital interactive environments, this typeface ideal for a wide range of uses, including data-rich apps, technical documentation and code editors.</p>
    <p>Currently under active development, Recursive will soon be available through Google Fonts. Until then, you can follow the project and download the latest betas on the <a href="https://www.github.com/arrowtype/recursive">Recursive project repo on GitHub</a>.</p>
  `,
  usage: `To meet the needs of global communication, Recursive supports a wide range of Latin-based languages, including Vietnamese. It also comes with an extended set of currencies, symbols, fractions, and arrows.`,
  chars: `AÄÁÂÀBCDÐEÈFGHIÎJKLMNOÒÓÔÕÖPQRSŞTUVWXYZabcdefghijklmnopqrstuvwxyÿz1234567890‰¹³²Ž‘?’“!”(%)[#]{@}/&\<-+÷×=>»«ØÆ&№₽₿¢¤$€ƒ₺₩↑↗→↘↓↙↔◊⁰⅜×₼₪₨₩∂₫%*`
})

font(11, {
  name: 'Overpass',
  provider: 'google',
  css: `font-family: 'Overpass', sans-serif;`,
  styles: '100,100i,200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i',
  subtitle: 'On the eleventh day of Christmas my true love gave to me an otherworldly retro road trip.',
  sample: 'Chestnuts roasting on an open fire; Jack Frost nipping at your nose.',
  author: {
    image: 'max-batt.jpg',
    name: 'Max Batt',
    title: 'Creator',
    company: 'Exo S.F.',
    quote: `Like TFJ's "Interstate," Overpass is inspired by the original US Highway Admin’s post-WWII-era roadway typeface, Highway Gothic. These great open roads of America are widely-referenced muses, but for me this typeface and the world it built conjures images of alien culture and ufology, whose origins come from the same time and place. One could also argue that the typeface itself is likewise universal, yet alien in certain details such as the truncated g and slanted ascenders. Far out!`,
    website: ''
  },
  designers: [
    {
      image: null,
      name: 'Delve Withrington',
      bio: null
    }
  ],
  about: `
    <p>
      An open source font family inspired by Highway Gothic.
      Sponsored by <a href="https://www.redhat.com/">Red Hat</a>.
      Created by <a href="https://www.delvefonts.com">Delve Fonts</a>.
    </p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzž1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(12, {
  name: 'Merriweather',
  provider: 'google',
  css: `font-family: 'Merriweather', serif;`,
  styles: '300,300i,400,400i,700,700i,900,900i',
  subtitle: 'On the twelfth day of Christmas my true love gave to me a charmingly diagonal sans serif.',
  sample: 'They never let poor Rudolph join in any reindeer games.',
  author: {
    image: 'arthur-yidi.jpg',
    name: 'Arthur Yidi',
    title: 'Product Designer',
    company: '',
    quote: `Very legible font that works great for reading long passages.`,
    website: 'https://arthuryidi.com/'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/QUi2_oQoDZ9NbI1tDvdSgY5k2dZXFKMZcrF7oeW3QnxC0eSnIVn5lzgQLzI5',
      name: 'Sorkin Type',
      bio: null
    }
  ],
  about: `
    <p>
      Merriweather was designed to be a text face that is pleasant to read on screens.
      It features a very large x height, slightly condensed letterforms, a mild diagonal stress, sturdy serifs and open forms.
    </p>
    <p>
      There is also <a href="https://fonts.google.com/specimen/Merriweather+Sans">Merriweather Sans</a>, a sans-serif version which closely harmonizes with the weights and styles of this serif family.
    </p>
    <p>
      The Merriweather project is led by Sorkin Type, a type design foundry based in Western Massachaussets, USA.
      To contribute, see <a href="https://github.com/EbenSorkin/Merriweather">github.com/EbenSorkin/Merriweather</a>
    </p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžАБВГҐДЂЕЁЄЖЗЅИІЇЙЈКЛЉМНЊОПРСТЋУЎФХЦЧЏШЩЪЫЬЭЮЯабĂÂÊÔƠƯăâêôơư1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(13, {
  trial: true,
  name: 'Aliens and Cows',
  provider: 'https://www.zetafonts.com/aliens-and-cows',
  fontFace: `
    @font-face {
      font-family: 'Aliens and Cows';
      src: url('/fonts/source/Aliens-and-Cows-Thin-trial.otf');
      font-weight: 100;
      font-style: normal;
      font-display: swap;
    }
    @font-face {
      font-family: 'Aliens and Cows';
      src: url('/fonts/source/Aliens-and-Cows-ExtraLight-trial.otf');
      font-weight: 200;
      font-style: normal;
      font-display: swap;
    }
    @font-face {
      font-family: 'Aliens and Cows';
      src: url('/fonts/source/Aliens-and-Cows-Light-trial.otf');
      font-weight: 300;
      font-style: normal;
      font-display: swap;
    }
    @font-face {
      font-family: 'Aliens and Cows';
      src: url('/fonts/source/Aliens-and-Cows-Regular-trial.otf');
      font-weight: 400;
      font-style: normal;
      font-display: swap;
    }
    @font-face {
      font-family: 'Aliens and Cows';
      src: url('/fonts/source/Aliens-and-Cows-Bold-trial.otf');
      font-weight: 700;
      font-style: normal;
      font-display: swap;
    }
    @font-face {
      font-family: 'Aliens and Cows';
      src: url('/fonts/source/Aliens-and-Cows-ExtraBold-trial.otf');
      font-weight: 800;
      font-style: normal;
      font-display: swap;
    }
    @font-face {
      font-family: 'Aliens and Cows';
      src: url('/fonts/source/Aliens-and-Cows-Heavy-trial.otf');
      font-weight: 900;
      font-style: normal;
      font-display: swap;
    }
  `,
  css: `font-family: 'Aliens and Cows', sans-serif;`,
  styles: '100,200,300,400,700,800,900',
  subtitle: 'On the thirteenth day of Christmas my true love gave to me a herd of gangly bovines and extraterrestrials.',
  sample: 'What fun it is to ride and sing a sleighing song tonight.',
  author: {
    image: 'laura-polkus.jpg',
    name: 'Laura Polkus',
    title: 'Designer',
    company: 'Freelance Creatrix',
    quote: `Aliens and Cows is such a fun font. I've always been drawn to condensed caps and I especially love the subtle curves on some of the terminals. It's strong without being too visually overwhelming and just a slight bit playful!`,
    website: 'https://laurapolkus.com/'
  },
  designers: [
    {
      image: null,
      name: 'Francesco Canovaro',
      bio: `
        <p>
          Canovaro is a passionate designer, type designer and art director, expert in web design and user experience.
          He is the <a href="https://www.kmzero.com/">Studio Kmzero</a> and <a href="https://www.zetafonts.com/">Zetafonts</a> font foundry co-owner and designer.
        </p>
      `
    }
  ],
  about: `
    <p>
      Designed by Francesco Canovaro, Aliens and Cows is an ultracondensed display typeface.
      It comes in one weights and features an extended character set with support for European languages, and contextual alternate for some characters.
    </p>
    <p>This trial font is TOTALLY FREE for NONCOMMERCIAL USES.</p>
    <p>
      For commercial uses you must get the full commercial typeface at:
      <a href="http://zetafonts.com/aliens-and-cows">http://zetafonts.com/aliens-and-cows</a>
    </p>
  `,
  usage: `Aliens and Cows covers over forty languages using the Latin alphabet as well as Greek and Cyrillic.`,
  chars: `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz‘?’“!”()[#]{@}/\\<-+÷×=>®©$€£¥¢₧™№:;,.*`
})

font(14, {
  name: 'BioRhyme',
  provider: 'google',
  css: `font-family: 'BioRhyme', serif;`,
  styles: '200,300,400,700,800',
  subtitle: 'On the fourteenth day of Christmas my true love gave to me a delightfully musical slab serif typeface.',
  sample: 'Where the treetops glisten and children listen to hear sleigh bells in the snow.',
  author: {
    image: 'christina-robinson.jpg',
    name: 'Christina Robinson',
    title: 'Email Marketing Manager',
    company: '',
    quote: `BioRyhme BioRhyme! It’s a slab serif which makes it different and interesting - I just learned that serifs are those little nubs at the end of a letter that make for greater legibility (if an old fashioned look). I’m also a biophiliac (someone who loves living things, and also the name of a Bjork album) and appreciate how the fractal quality of these letters echo (or perhaps... rhyme with) the image of a tree in winter, looking dead but just dormant, waiting for spring to arrive, so its fuzzy buds can jut out. Plus, that squiggly “z” is pretty funky!`,
    website: 'https://www.linkedin.com/in/christinattrobinson'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/YJscrD5zPvjZWNSKgxQcI3B6tTRxkbcPCYlWNFoN5OHqkcRPi_RjCpLorr4',
      name: 'Aoife Mooney',
      bio: `
        <p>Aoife is a typeface designer and teacher. She has a degree in Visual Communications from Dublin Institute of Technology and an MA in Typeface Design from the University of Reading. Alongside her freelance practice, Aoife is an Assistant Professor at Kent State University, where she teaches typography and typeface design. Before moving to Ohio, Aoife worked as part of Hoefler & Co.’s design team in New York, developing Idlewild, Surveyor, and many other typefaces. Most recently she worked with Frere-Jones Type on Mallory. BioRhyme is her first original, published typeface design.</p>
        <p><a href="http://www.aoifemooney.org/">www.aoifemooney.org</a> | <a href="https://github.com/aoifemooney">GitHub</a> | <a href="https://twitter.com/aoifemooney">Twitter</a></p>
      `
    }
  ],
  about: `
    <p>
      BioRhyme is a Latin typeface family comprised of two widths, a normal family and an <a href="https://fonts.google.com/specimen/BioRhyme+Expanded">expanded</a> family.
      Each family has 5 weights, and both are intended for use in large and medium sizes.
    </p>
    <p>To contribute, see <a href="https://github.com/aoifemooney/makingbiorhyme">github.com/aoifemooney/makingbiorhyme</a></p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzž1234567890‘?’“!”(%)[#]{@}/&\<-+×=>®©$€£¥¢:;,.*`
})

font(15, {
  name: 'September Script',
  provider: 'https://fontbundles.net/free-fonts/regular-fonts/september-script',
  fontFace: `
    @font-face {
      font-family: 'September Script';
      src: url('/fonts/source/SeptemberScript-Regular.woff') format('woff');
      font-weight: normal;
      font-style: normal;
    }
  `,
  css: `font-family: 'September Script', sans-serif;`,
  styles: '400',
  subtitle: 'On the fifteenth day of Christmas my true love gave to me an exquisitely sumptuousness script.',
  sample: `Altho' it's been said many times, many ways; "Merry Christmas to you"`,
  author: {
    image: 'andy-croft.jpg',
    name: 'Andy Croft',
    title: 'Director and Founder',
    company: 'Fontbundles.net',
    quote: `I'd choose September Script because it's versatile, professional and has lots of extra glyphs included to make your text truly unique each time!`,
    website: 'https://fontbundles.net/'
  },
  designers: [
    {
      image: null,
      name: 'Font Bundles',
      bio: null
    }
  ],
  about: `
    <p>Watch the Sunset with September Script. A fantastic Script font which oozes elegance and style.</p>
    <p>PUA encoded and with lots of alternates, swirls and swashes September Script is a must have font for any typography enthusiast.</p>
    <p>Exclusive to FontBundles.net September Script is a fantastic addition to any collection.</p>
  `,
  chars: `AÄÁÂÀBCDÐEÈFGHIÎJKLMNOÒÓÔÕÖPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890‰¹³²Ž‘?’“!”(%)[#]{@}/&\<-+÷×=>»«ØÆtæ•º°±®©$€£¥¢:;,.*`
})

font(16, {
  name: 'Pacifico',
  provider: 'google',
  css: `font-family: 'Pacifico', cursive;`,
  styles: '400',
  subtitle: 'On the sixteenth day of Christmas my true love gave to me a joyous handwriting brush script.',
  sample: 'Everybody knows a turkey and some mistletoe help to make the season bright',
  author: {
    image: 'angie.jpg',
    name: 'Angie La Porte-Domkus',
    title: '',
    company: '',
    quote: `I think Pacifico is rightly named. It brings a carefree quality to the words, with its loopy script. It seems almost breezy, like it belongs next to an image of a hammock between two palm trees.`,
    website: ''
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/PPz3vY4bIHZg-3LvvXP3F4WbtesyOBbfZZNIQ270-76RjLzj6QiJm54A9_Xg',
      name: 'Vernon Adams',
      bio: `
        <p>A lifelong artist, Vernon practiced typeface design from 2007 to 2014 in which he eagerly explored designing type for the cloud-based era. His work spans all genres, from lively script faces to workhorse text families and operating system UI. Vernon graduated with an MA in Typeface Design from the University of Reading and lives in California. Follow his story at <a href="http://sansoxygen.com/">www.sansoxygen.com</a>.</p>
        <p><a href="https://github.com/vernnobile">Github</a> | <a href="https://www.instagram.com/explore/tags/sansoxygen/">Instagram</a></p>
      `
    },
    {
      image: null,
      name: 'Jacques Le Bailly',
      bio: null
    },
    {
      image: null,
      name: 'Botjo Nikoltchev',
      bio: null
    },
    {
      image: null,
      name: 'Ani Petrova',
      bio: null
    }
  ],
  about: `
    <p>
      Aloha!
      Pacifico is an original and fun brush script handwriting font that was inspired by the 1950s American surf culture in 2011.
      It was redrawn by Jacques Le Bailly at <a href="http://www.baronvonfonthausen.com/">Baron von Fonthausen</a> in 2016.
      It was expanded to Cyrillic by Botjo Nikoltchev and Ani Petrova at <a href="https://lettersoup.de">Lettersoup</a> in 2017.
    </p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžАБВГҐДЂЕЁЄЖЗЅИІЇЙЈКЛЉМНЊОПРСТЋУЎФХЦЧЏШЩЪЫЬЭЮЯĂÂÊÔƠƯăâêôơư1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(17, {
  name: 'Montserrat',
  provider: 'google',
  css: `font-family: 'Montserrat', sans-serif;`,
  styles: '100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i',
  subtitle: 'On the seventeenth day of Christmas my true love gave to me a sans serif inspired by the urban signage of Buenas Aires.',
  sample: 'All of the other reindeer used to laugh and call him names. ',
  author: {
    image: 'frank.jpg',
    name: 'Frank Javier Tomayo Puentes',
    title: 'Designer',
    company: 'Inverxus',
    quote: `A typeface really versatile for web and print with a modern and clean design. It has so much energy attached to the shapes and a lot of impact; you see it and fall in love right away.`,
    website: 'https://www.instagram.com/frankjaviertamayo/'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/a1ixVxrGmNMyZQzukXonkfK6f-Ivk1paYsM-nUn4KeulS_hL-ySoWmjYy2k',
      name: 'Julieta Ulanovsky',
      bio: `
        <p>Julieta is a graphic designer and owner of <a href="http://zkysky.com.ar/">ZkySky</a>, a design studio which she co-founded in 1989 after earning a degree in Typeface Design. She lives and works in Montserrat, the first and oldest neighborhood in Buenos Aires. Julieta admires many type designers including Harald Geisler, and fellow Argentines  Juan Pablo del Peral and Alejandro Paul. She is currently developing new variants of Montserrat—italics, plus new weights and styles—and dreams that it will soon become a large, extended family.</p>
        <p><a href="https://github.com/JulietaUla/Montserrat">GitHub</a></p>
      `
    },
    {
      image: 'https://lh3.googleusercontent.com/tcm4V7e-qi3EYYca3oxNz0re3YHXcnl6Pq3u4UQhBD9QFEykG1i2gD3cg0g',
      name: 'Sol Matas',
      bio: null
    },
    {
      image: 'https://lh3.googleusercontent.com/kQaDLEwtd9Uf1PWTQgTy12r63cN5xn2QOPIa6HRJQX2Pc_dMRWxnai3RDJoJ',
      name: 'Juan Pablo del Peral',
      bio: null
    },
    {
      image: null,
      name: 'Jacques Le Bailly',
      bio: null
    }
  ],
  about: `
    <p>
      The old posters and signs in the traditional Montserrat neighborhood of Buenos Aires inspired Julieta Ulanovsky to design this typeface and rescue the beauty of urban typography that emerged in the first half of the twentieth century.
      As urban development changes that place, it will never return to its original form and loses forever the designs that are so special and unique.
      The letters that inspired this project have work, dedication, care, color, contrast, light and life, day and night!
      These are the types that make the city look so beautiful.
      The Montserrat Project began with the idea to rescue what is in Montserrat and set it free under a libre license, the SIL Open Font License.
    </p>
    <p>
      The family has two sisters, <a href="http://fonts.google.com/specimen/Montserrat+Alternates">Alternates</a> and <a href="http://fonts.google.com/specimen/Montserrat+Subrayada">Subrayada</a>.
      Many of the letterforms are special in the Alternates family, while 'Subrayada' means 'Underlined' in Spanish and celebrates a special style of underline that is integrated into the letterforms found in the <em>Montserrat</em> neighborhood.
    </p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžАБВГҐДЂЕЁЄЖЗЅИІЇЙЈКЛЉМНЊОПРСТЋУЎФХЦЧЏШЩЪЫЬЭЮЯабвгґдђеёєжзѕиіїйјклљмнњопрстћуўфхцчџшщъыьэюяĂÂÊÔƠƯăâêôơư1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(18, {
  name: 'Quicksand',
  provider: 'google',
  css: `font-family: 'Quicksand', sans-serif;`,
  styles: '300,400,500,600,700',
  subtitle: 'On the eighteenth day of Christmas my true love gave to me a lusciously rounded geometric sans serif parade.',
  sample: 'Not only in the summertime, but even in winter is thy prime. ',
  author: {
    image: 'ashley-bonnard.jpg',
    name: 'Ashley Bonnard',
    title: 'Product Designer',
    company: 'Walmart',
    quote: `I love Quicksand because it is very legible, modern and uses geometric shapes as a core foundation, similar to Gotham (another favorite of mine). I would describe this font as playfully elegant. Most of the letterforms are very clean and geometric, but the tail of the capital Q has a beautiful curvature to it giving it just that hint of elegance. This font was originally designed as a display font, thus there is no italicized version of it—however the regular version is still very legible even at small sizes. I discovered it while looking for a free alternative to Gotham and noticed that it looked very similar to rounded version of Gotham but with just a bit more flare to it.`,
    website: 'https://www.ashleybonnard.com/'
  },
  designers: [
    {
      image: null,
      name: 'Andrew Paglinawan',
      bio: null
    }
  ],
  about: `
    <p>
      Quicksand is a display sans serif with rounded terminals.
      The project was initiated by Andrew Paglinawan in 2008 using geometric shapes as a core foundation.
      It is designed for display purposes but kept legible enough to use in small sizes as well.
      In 2016, in collaboration with Andrew, it was thoroughly revised by Thomas Jockin to improve the quality.
      In 2019, Mirko Velimirovic converted the family into a variable font.
    </p>
    <p>To contribute, see <a href="https://github.com/andrew-paglinawan/QuicksandFamily">github.com/andrew-paglinawan/QuicksandFamily</a>.</p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžĂÂÊÔƠƯăâêôơư1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(19, {
  name: 'Karla',
  provider: 'google',
  css: `font-family: 'Karla', sans-serif;`,
  styles: '400,400i,700,700i',
  subtitle: 'On the nineteenth day of Christmas my true love gave to me an easy-going delectable grotesque.',
  sample: 'Down thru the chimney with lots of toys all for the little ones Christmas joys.',
  author: {
    image: 'hector-ayon.jpg',
    name: 'Hector Ayon',
    title: 'UX and Visual Designer',
    company: '',
    quote: `I like Karla because it doesn’t take itself too seriously and feels pleasantly modern!`,
    website: ''
  },
  designers: [
    {
      image: null,
      name: 'Jonny Pinhorn',
      bio: `
        <p>After completing an MA in Type Design at the University of Reading, Jonny went on to design Karla for Google Fonts. Karla is a popular and quirky sans-serif typeface that supports both Latin and Tamil scripts. His continued fascination with India and Indian languages led him to ITF, where he worked for three years. Jonny continues to work exclusively on Indic scripts—including <a href="https://github.com/jonpinhorn/shrikhand">Shrikhand</a> and <a href="https://github.com/jonpinhorn/atithi">Atithi</a> most recently.</p>
        <p><a href="http://jonnypinhorn.co.uk/">Jonnypinhorn.co.uk</a> | <a href="https://github.com/jonpinhorn">GitHub</a> | <a href="https://twitter.com/jonpinhorn_type">Twitter</a></p>
      `
    }
  ],
  about: `
    <p>
      Karla is a grotesque sans serif typeface family that supports languages
      that use the Latin script and the Tamil script. This is the Latin script
      part of the family, with Roman and Italic styles in two weights, Regular and Bold.
    </p>
    <p>The Tamil parts are available from the <a href="https://fonts.google.com/earlyaccess">Early Access</a> page.</p>
    <p>To contribute to the project contact <a href="mailto:jonpinhorn.typedesign@gmail.com">Jonathan Pinhorn</a>.</p>
  `,
  chars: `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890‘?’!(%)[#]{@}/&\<-+=>$£:;,.*`
})

font(20, {
  name: 'Barlow',
  provider: 'google',
  css: `font-family: 'Barlow', sans-serif;`,
  styles: '100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i',
  subtitle: 'On the twentieth day of Christmas my true love gave to me a confident geometric sans serif.',
  sample: `You're as cuddly as a cactus, you're as charming as an eel, Mr. Grinch.`,
  author: {
    image: 'tania-r.jpg',
    name: 'Tania Richardson',
    title: 'Senior UX/UI Designer',
    company: 'Octopus Deploy',
    quote: `I'm really loving Barlow at the moment. If you use Roboto in your brand guidelines like we do at Octopus Deploy, it pairs nicely in web headings and posters. I find myself using it more in brands that want a friendly looking, condensed sans serif that is also easy to read. That's because Barlow shares qualities from font used in car plates and highway signs, so it's perfect for anything from paragraphs to headings. Being a free Google font, Barlow can be easily served on the web and has a nice range of styles to choose from.`,
    website: 'https://taniarichardson.com/'
  },
  designers: [
    {
      image: null,
      name: 'Jeremy Tribby',
      bio: null
    }
  ],
  about: `
    <p>
      Barlow is a slightly rounded, low-contrast, grotesk type family. Drawing from the visual style of the California public, Barlow shares qualities with the state's car plates, highway signs, busses, and trains.
      This is the Normal family, which is part of the superfamily along with Semi Condensed and Condensed, each with 9 weights in Roman and Italic.
    </p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžĂÂÊÔƠƯăâêôơư1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(21, {
  name: 'Brandon Grotesque',
  provider: 'https://www.cufonfonts.com/font/brandon-grotesque',
  fontFace: `
    @font-face {
      font-family: 'Brandon Grotesque';
      font-weight: 100;
      src: url('/fonts/source/Brandon_thin.woff') format('woff');
    }
    @font-face {
      font-family: 'Brandon Grotesque';
      font-weight: 100;
      font-style: italic;
      src: url('/fonts/source/Brandon_thin_it.woff') format('woff');
    }
    @font-face {
      font-family: 'Brandon Grotesque';
      font-weight: 300;
      src: url('/fonts/source/Brandon_light.woff') format('woff');
    }
    @font-face {
      font-family: 'Brandon Grotesque';
      font-weight: 300;
      font-style: italic;
      src: url('/fonts/source/Brandon_light_it.woff') format('woff');
    }
    @font-face {
      font-family: 'Brandon Grotesque';
      font-weight: 400;
      src: url('/fonts/source/Brandon_reg.woff') format('woff');
    }
    @font-face {
      font-family: 'Brandon Grotesque';
      font-weight: 400;
      font-style: italic;
      src: url('/fonts/source/Brandon_reg_it.woff') format('woff');
    }
    @font-face {
      font-family: 'Brandon Grotesque';
      font-weight: 500;
      src: url('/fonts/source/Brandon_med.woff') format('woff');
    }
    @font-face {
      font-family: 'Brandon Grotesque';
      font-weight: 500;
      font-style: italic;
      src: url('/fonts/source/Brandon_med_it.woff') format('woff');
    }
    @font-face {
      font-family: 'Brandon Grotesque';
      font-weight: 700;
      src: url('/fonts/source/Brandon_bld.woff') format('woff');
    }
    @font-face {
      font-family: 'Brandon Grotesque';
      font-weight: 700;
      font-style: italic;
      src: url('/fonts/source/Brandon_bld_it.woff') format('woff');
    }
    @font-face {
      font-family: 'Brandon Grotesque';
      font-weight: 900;
      src: url('/fonts/source/Brandon_blk.woff') format('woff');
    }
    @font-face {
      font-family: 'Brandon Grotesque';
      font-weight: 900;
      font-style: italic;
      src: url('/fonts/source/Brandon_blk_it.woff') format('woff');
    }
  `,
  css: `font-family: 'Brandon Grotesque', sans-serif;`,
  styles: '100,100i,300,300i,400,400i,500,500i,700,700i,900,900i',
  subtitle: 'On the twenty-first day of Christmas my true love gave to me a typographic expression of the California public.',
  sample: 'Grandma got run over by a reindeer walking home from our house Christmas eve.',
  author: {
    image: 'christian-b.jpg',
    name: 'Christian Bergstrom',
    title: 'Produt Designer',
    company: 'Clover',
    quote: `I fell in love with Brandon Grotesque because it achieved a unique balance between modern and warm/playful. Its a geometric font that is inspired by the inaccurate printing methods in 1930s magazines that would result in slight rounding of corners during the ink press process. It feels bold and utilitarian with its geometric shapes and many weights & alternate styles, while being playful and spontaneous`,
    website: 'https://fonts.adobe.com/designers/hannes-von-dohren'
  },
  designers: [
    {
      image: '/fonts/designers/hannes-von-dohren.jpg',
      name: 'Hannes von Döhren',
      bio: `
        <p>After completing his studies (graphic design) in 2005, Hannes von Döhren worked in an advertising agency in Hamburg, Germany. Since 2007 he has been working as a freelance graphic- and type designer in Berlin. Von Döhren previously released several typefaces through Linotype, ITC and T26. He now releases some typefaces through his HVD Fonts foundry.</p>
      `
    }
  ],
  about: `
    <p>
      Designed by <a href="https://fonts.adobe.com/designers/hannes-von-dohren">Hannes von Döhren</a>.
      From <a href="https://fonts.adobe.com/foundries/hvd-fonts">HVD Fonts</a>.
    </p>
  `,
  usage: 'This sample by Ian Shei highlights its Bauhaus-inspired geometry coupled with "softness and warmth".'
})

font(22, {
  name: 'Teko',
  provider: 'google',
  css: `font-family: 'Teko', sans-serif;`,
  styles: '300,400,500,600,700',
  subtitle: 'On the twenty-second day of Christmas my true love gave to me a typeface curated to scale to miraculous magnitudes on large screens.',
  sample: `Jolly old Saint Nicholas, Lean your ear this way! Don't you tell a single soul what I'm going to say.`,
  author: {
    image: 'jay-henry.jpg',
    name: 'Jay Henry',
    title: 'President and CEO',
    company: 'Ultra Design Agency',
    quote: `I have been loving Teko lately as we are using it for a defense contractor’s project. It is masculine, but not overwhelmingly so. It looks great in both upper and lowercase and is easily utilized as it is a Google font.`,
    website: 'https://ultradesignagency.com/'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/HSnoeY6FyQyd6EFmIpms55KH5EjGRqCzLAKillb2DqEYsW1wjy0IEyCmrK2r',
      name: 'Indian Type Foundry',
      bio: `
        <p><a href="http://www.indiantypefoundry.com/">Indian Type Foundry</a> (ITF) creates retail and custom multilingual fonts for print and digital media. Started in 2009 by Satya Rajpurohit and Peter Bil’ak, ITF works with designers from across the world. ITF fonts are used by clients ranging from tech giants like Apple, Google, and Sony, to various international brands.</p>
        <p><a href="http://github.com/itfoundry">Github</a> | <a href="https://twitter.com/itfoundry">Twitter</a></p>
      `
    }
  ],
  about: `
    <p>Teko is an Open Source typeface that currently supports the Devanagari and Latin scripts. This font family has been created for use in headlines and other display-sized text on screen. Five font styles make up the initial release.</p>
    <p>The Teko typeface features letterforms with low stroke contrast, square proportions and a structure that appears visually simple. At display sizes, Teko works equally well on screen or in print. Each font contains 1090 glyphs, offering full support for the conjuncts and ligatures required by languages written with the Devanagari script. Teko is an excellent choice for use in advertising or for news tickers on television screens.</p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžआईऊऋॠऌॡऐऔऎअंअँकखगघङचछजझञटठडढणतथदधनपफबभयरवळशषसह1234567890₹०१२३४५६७८९‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(23, {
  name: 'Space Mono',
  provider: 'google',
  css: `font-family: 'Space Mono', monospace;`,
  styles: '400,400i,700,700i',
  subtitle: 'On the twenty-third day of Christmas my true love gave to me a Eurostyle influenced fixed-width headline typeface.',
  sample: 'Give her a dolly that laughs and cries; One that will open and shut her eyes.',
  author: {
    image: 'elina-lin.jpg',
    name: 'Elina Lin',
    title: 'Product Designer',
    company: 'Linkedin',
    quote: `Monospace fonts usually don't garnish too much excitement in the type world; they are bland with fixed-widths, characterized by its function and construction for readability at small point sizes. That's why Space Mono took me by surprise with its familiar, yet unconventional personality. One of my favorite features of Space Mono is the contrast reflected within every letter, as seen in its friendly upper half to the dramatic 90-degree pipe bends of its descenders.`,
    website: 'https://www.elinalin.com/'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/P6KopN6jpw_bkJJgJ6c5fDq3L--u5bKzHcsHS-N7AKkh26SX2iadr3XPNHA',
      name: 'Colophon',
      bio: `
        <p>Colophon Foundry is a London and Los Angeles-based digital type foundry established in 2009. Its members comprise Benjamin Critton (US), Edd Harrington (UK), and Anthony Sheret (UK). The foundry's commissioned work in type design is complemented by independent and interdependent initiatives in editorial design, publishing, curation, and pedagogy.</p>
        <p><a href="http://www.colophon-foundry.org">colophon-foundry.org</a></p>
      `
    }
  ],
  about: `
    <p>Space Mono is an original fixed-width type family designed by Colophon Foundry for Google Design. It supports a Latin Extended glyph set, enabling typesetting for English and other Western European languages.</p>
    <p>Developed for editorial use in headline and display typography, the letterforms infuse a geometric foundation and grotesque details with qualities often found in headline typefaces of the 1960s, many of which have since been co-opted by science fiction films, television, and literature.</p>
    <p>Typographic features include old-style figures, superscript and subscript numerals, fractions, center-height and cap-height currency symbols, directional arrows, and multiple stylistic alternates.</p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžĂÂÊÔƠƯăâêôơư1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(24, {
  name: 'DM Serif Text',
  provider: 'google',
  css: `font-family: 'DM Serif Text', serif;`,
  styles: '400,400i',
  subtitle: 'On the twenty-fourth day of Christmas my true love gave to me a robustly dignified serif family.',
  sample: 'We three kings of Orient are bearing gifts we traverse afar.',
  author: {
    image: 'philippe-g.jpg',
    name: 'Philippe Gauthier',
    title: 'Design Lead',
    company: 'Ueno SF',
    quote: `I’m a big fan of Colophon Foundry so when I saw that they partnered with Google to publish new open-source typefaces on Google Fonts, I got very curious. The one that stood out for me was DM Serif Text. It's a beautifully constructed and balanced font that has some very nice details, especially the terminals. I mean, look at that lower case ‘’a’’! I’m definitely looking forward to use it on a project.`,
    website: 'https://dribbble.com/philgauthier'
  },
  designers: [
    {
      image: null,
      name: 'Colophon Foundry',
      bio: `
        <p>The DM Serif project was commissoned by Google from Colophon, an international and award-winning type foundry based in London (UK) and Los Angeles (US) who publish and distribute high-quality retail and custom typefaces for analog and digital media.</p>
      `
    },
    {
      image: null,
      name: 'Frank Grießhammer',
      bio: null
    }
  ],
  about: `
    <p>
      DM Serif Text is a lower-contrast counterpart to the high-contrast <a href="https://fonts.google.com/specimen/DM+Serif+Display">DM Serif Display</a>.
      While the serifs remain delicate, the DM Serif Text family is a more robust variant of the Display sibling, intended for smaller sub-headings and text sizes.
    </p>
    <p>
      DM Serif Text supports a Latin Extended glyph set, enabling typesetting for English and other Western European languages.
      It was designed by Colophon Foundry (UK), that started from the Latin portion of Adobe <a href="https://fonts.google.com/specimen/Source+Serif+Pro">Source Serif Pro</a>, by Frank Grießhammer.
    </p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzž1234567890‘?’“!”(%)[#]{@}&©$€£¥¢:;,.*`
})

font(25, {
  name: 'Lobster',
  provider: 'google',
  css: `font-family: 'Lobster', cursive;`,
  styles: '400',
  subtitle: 'On the twenty-fifth day of Christmas my true love gave to me a playfully articulate handwriting font.',
  sample: 'I saw Mommy tickle Santa Claus underneath the mistletoe last night.',
  author: {
    image: 'benjamin-hersh.jpg',
    name: 'Benjamin Hersh',
    title: 'Product Designer',
    company: 'Dropbox',
    quote: `Lobster is Santa Claus in typographic form. Joyful, generous, commercially over-used. Believe in it, and you will stay a child at heart forever.`,
    website: 'https://benjaminhersh.com/'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/iSUD5nw1B1oGvNQX5f-Gn_OOGyHywp4EF-T1CJBLRTyxqlwpKPi70m7gA5b-',
      name: 'Impallari Type',
      bio: null
    },
    {
      image: 'https://lh3.googleusercontent.com/8_Ko9pfyjgJZcRYFkYxecsGSTs5ybw8025-aYOi_M2KX4-L0fAkRY0PFnUk',
      name: 'Cyreal',
      bio: null
    }
  ],
  about: `
    <p>
      The Lobster font took a different approach.
      The new OpenType format gives us the possibility to have multiple versions of each letter, and that's exactly what we are doing: Instead of compromising the design of our letters to force connections, we do what lettering artist do.
      We draw many versions of each letter and a lot of different letter-pairs (aka "ligatures") so we always use the best possible variation of each letter depending of the context of the letter inside each word.
      All this happens automatically in any browser that supports ligatures.
    </p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžАБВГҐДЂЕЁЄЖЗЅИІЇЙЈКЛЉМНЊОПРСТЋУЎФХЦЧЏШЩЪЫЬЭЮЯабвгґдђеёєжзѕиіїйјклљмнњопрстћуўфхцчџшщъыьэюяĂÂÊÔƠƯăâêôơư1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(26, {
  name: 'Muli',
  provider: 'google',
  css: `font-family: 'Muli', sans-serif;`,
  styles: '200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i',
  subtitle: 'On the twenty-sixth day of Christmas my true love gave to me an refreshingly minimalist sans serif.',
  sample: 'And the thing that will make them ring is the carol that you sing right within your heart.',
  author: {
    image: 'yamini.jpg',
    name: 'Yamini Bhatt',
    title: 'Senior Product Engineer',
    company: 'Webcor',
    quote: `Muli is clear, simple, and easy to read. All characters are balanced and sharp—a very versatile font."`,
    website: 'https://www.linkedin.com/in/yaminib/'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/PPz3vY4bIHZg-3LvvXP3F4WbtesyOBbfZZNIQ270-76RjLzj6QiJm54A9_Xg',
      name: 'Vernon Adams',
      bio: `
        <p>Vernon practiced typeface design from 2007 to 2014. A lifelong artist, during this time he eagerly explored designing type for the cloud-based era. His work spans all genres, from lively script faces to workhorse text families and operating system UI. Vernon graduated with an MA in Typeface Design from the University of Reading and lives in California. His designs are mostly published as open source Google Fonts and his favorite projects include Oxygen Mono, Monda, and Bowlby One. Follow his story at <a href="http://sansoxygen.com/">www.sansoxygen.com</a>.</p>
        <p><a href="https://github.com/vernnobile">Github</a> | <a href="https://www.instagram.com/explore/tags/sansoxygen/">Instagram</a></p>
      `
    }
  ],
  about: `
    <p>Muli is a minimalist Sans Serif typeface, designed for both display and text typography. </p>
    <p>
      Since the initial launch in 2011, Muli was updated continually by Vernon Adams until 2014.
      Vernon added more weights, support for more Latin languages, tightened the spacing and kerning and made many glyph refinements throughout the family based on hundreds of users' feedback.
      In 2017 the family was updated by Jacques Le Bailly to complete the work started by Vernon after he passed away, in collaboration with his wife Allison, an arist who holds the trademark on the typeface family name.
      In August 2019, it was updated with a Variable Font "Weight" axis.
    </p>
    <p>To contribute, see <a href="https://github.com/googlefonts/MuliFont">github.com/googlefonts/MuliFont</a></p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžĂÂÊÔƠƯăâêôơư1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(27, {
  name: 'Bebas Neue',
  provider: 'google',
  css: `font-family: 'Bebas Neue', sans-serif;`,
  styles: '400',
  subtitle: 'On the twenty-seventh day of Christmas my true love gave to me a graceful condensed sans seriif filled with technical straightforwardness and simple warmth.',
  sample: 'He knows when you’ve been bad or good, So be good for goodness sake!',
  author: {
    image: 'nicole.jpg',
    name: 'Nicole Alonso',
    title: 'Artist',
    company: 'Studio Neex',
    quote: `With an extended character set, this font can be used through multiple languages. It possessed clean lines and a simplistic bold feel that is ideal for descriptions, headlines and packaging.`,
    website: 'https://www.studioneex.com/'
  },
  designers: [
    {
      image: null,
      name: 'Ryoichi Tsunekawa',
      bio: null
    }
  ],
  about: `
    <p>Bebas Neue is a display family suitable for headlines, captions, and packaging, designed by Ryoichi Tsunekawa. It's based on the original Bebas typeface. The family is suitable for pro users due to its extended character set and OpenType features.</p>
    <p>To contribute, see <a href="https://github.com/dharmatype/Bebas-Neue">https://github.com/dharmatype/Bebas-Neue</a>.</p>
    <p><a href="http://bebasneue.com">http://bebasneue.com</a></p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzž1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(28, {
  name: 'Abril Fatface',
  provider: 'google',
  css: `font-family: 'Abril Fatface', serif;`,
  styles: '400',
  subtitle: 'On the twenty-eighth day of Christmas my true love gave to me a cosmopolitan Didone style serif that draws its inspiration from the posters of Victorian Paris.',
  sample: 'He had a broad face and a little round belly that shook when he laughed, like a bowl full of jelly.',
  author: {
    image: 'eddie.png',
    name: 'Eddie Corral',
    title: 'Illustrator',
    company: null,
    quote: `I like Abril Fatface because it's both sophisticated and fun. Also, the name reminds me of my old boss.`,
    website: 'https://www.instagram.com/edweird_see/'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/d57BD3LuAypRG-8IQf2xKbeIMYwr4PSup6AD8UKsSzt8-wYf5lpSWMw7VUw',
      name: 'TypeTogether',
      bio: `
        <p><a href="http://www.type-together.com">TypeTogether</a> is an independent type foundry committed to excellence in type design with a focus on editorial use. Known for the popular typefaces <a href="http://www.type-together.com/Adelle">Adelle</a> and <a href="http://www.type-together.com/Bree">Bree</a>, TypeTogether also creates custom type designs for corporate use, including their work for <a href="http://www.type-together.com/index.php?action=portal/viewContent&cntId_content=3301&id_section=141">Google Play Books</a>, <a href="http://www.type-together.com/index.php?action=portal/viewContent&cntId_content=3230&id_section=141">Clarín</a>, and <a href="http://www.type-together.com/index.php?action=portal/viewContent&cntId_content=3198&id_section=141">Apple</a>.</p>
        <p><a href="https://twitter.com/TypeTogether">Twitter</a></p>
      `
    }
  ],
  about: `
    <p>Abril Fatface is part of a bigger type family system, Abril, which includes 18 styles for all Display and Text uses. The titling weights are a contemporary revamp of classic Didone styles, display both neutrality and strong presence on the page to attract reader attention with measured tension by its curves, good color and high contrast.</p>
    <p>Abril Fatface in particular is inspired by the heavy titling fonts used in advertising posters in 19th century Britain and France. The thin serifs and clean curves lend the typeface a refined touch that give any headline an elegant appearance. The Extended Latin character set supports over 50 languages, including those from Central and Northern Europe.</p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzž1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(29, {
  name: 'Poppins',
  provider: 'google',
  css: `font-family: 'Poppins', sans-serif;`,
  styles: '100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i',
  subtitle: 'On the twenty-ninth day of Christmas my true love gave to me an ultra-clean "well-rounded" sans serif based on the geometry of circles.',
  sample: `I'll have a blue Christmas without you; I'll be so blue thinking about you.`,
  author: {
    image: 'jacob.jpg',
    name: 'Jacob Cass',
    title: 'Graphic Designer',
    company: 'Just Creative',
    quote: `Poppins is full of character. It's clean & fresh and brings an avant-garde feel to a project without being too overt. It's friendly and packs a punch, especially with the bolder weights!`,
    website: 'https://justcreative.com/'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/HSnoeY6FyQyd6EFmIpms55KH5EjGRqCzLAKillb2DqEYsW1wjy0IEyCmrK2r',
      name: 'Indian Type Foundry',
      bio: `
        <p><a href="http://www.indiantypefoundry.com/">Indian Type Foundry</a> (ITF) creates retail and custom multilingual fonts for print and digital media. Started in 2009 by Satya Rajpurohit and Peter Bil’ak, ITF works with designers from across the world. ITF fonts are used by clients ranging from tech giants like Apple, Google, and Sony, to various international brands.</p>
        <p><a href="http://github.com/itfoundry">Github</a> | <a href="https://twitter.com/itfoundry">Twitter</a></p>
      `
    }
  ],
  about: `
    <p>
      Geometric sans serif typefaces have been a popular design tool ever since these actors took to the world’s stage.
      Poppins is one of the new comers to this long tradition.
      With support for the Devanagari and Latin writing systems, it is an internationalist take on the genre.
    </p>
    <p>
      Many of the Latin glyphs (such as the ampersand) are more constructed and rationalist than is typical.
      The Devanagari design is particularly new, and is the first ever Devanagari typeface with a range of weights in this genre.
      Just like the Latin, the Devanagari is based on pure geometry, particularly circles.
    </p>
    <p>
      Each letterform is nearly monolinear, with optical corrections applied to stroke joints where necessary to maintain an even typographic color.
      The Devanagari base character height and the Latin ascender height are equal; Latin capital letters are shorter than the Devanagari characters, and the Latin x-height is set rather high.
    </p>
    <p>
      The Devanagari is designed by Ninad Kale.
      The Latin is by Jonny Pinhorn.
      To contribute, see <a href="https://github.com/itfoundry/poppins">github.com/itfoundry/poppins</a>
    </p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžआईऊऋॠऌॡऐऔअंअँकखगघङचछजझञटठडढणतथदधनपफबभयरवळशषसह1234567890₹०१२३४५६७८९‘?’“!”₹(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

font(30, {
  name: 'Ostrich Sans Heavy',
  provider: 'https://open-foundry.com/fonts/ostrich_sans_heavy',
  fontFace: `
    @font-face {
      font-family: 'Ostrich Sans Heavy';
      font-weight: normal;
      src: url('/fonts/source/ostrich-heavy.otf');
    }
  `,
  css: `font-family: 'Ostrich Sans Heavy', sans-serif;`,
  styles: '400',
  subtitle: 'On the thirtieth day of Christmas my true love gave to me a long-necked, swift-running typeface perfect for any message that deserves to look attractive.',
  sample: 'If the Fates allow, hang a shining star upon the highest bough.',
  author: {
    image: 'michael-solorio.jpg',
    name: 'Michael Solorio',
    title: 'Designer',
    company: 'M Solo Designs',
    quote: `Ostrich has a nice eye-catching effect without being too serious, with its rounded edges. This font is something I would use as a heading to draw people in.`,
    website: 'https://www.msolodesigns.com/'
  },
  designers: [
    {
      image: null,
      name: 'Tyler Finck',
      bio: `<p><a href="https://www.tylerfinck.com/">Portfolio</a></p>`
    }
  ],
  about: `
    <p>Ostrich Sans Heavy is a gorgeous modern sans serif with a very long neck. Ostrich Sans was created by Tyler Finck and is currently distributed by The League of Moveable Type. It was initially submitted to us by Open Foundry. It excels in display purposes, posters, headlines, attractive messages, banners, prints, and other creative projects.</p>
    <p>Heavy is a Sans Serif cut of the Ostrich Sans family, which consists of 9 different styles: Dashed Thin, Rounded Medium, Ultra Light, Normal, Bold, Black, Inline, Inline Italics and Heavy Round.</p>
  `,
  usage: 'Ostrich also includes a whole slew of other styles, with various weights for each.'
})

font(31, {
  name: 'Bree Serif',
  provider: 'google',
  css: `font-family: 'Bree Serif', serif;`,
  styles: '400',
  subtitle: 'On the thirty-first day of Christmas my true love gave to me a gregariously chummy, fun-loving slab serif.',
  sample: 'O star of wonder, star of night—star with royal beauty bright.',
  author: {
    image: 'lydia.jpg',
    name: 'Lydia Weber',
    title: 'Designer',
    company: 'Leftfield Labs @ Google',
    quote: `Voted most likely to be the last one grooving on the house party dancefloor, Bree Serif is high spirited, spry, and full of personality. This slab serif is both energetic and mature, and boasts elements of its uprihgt sans italic cousin, Bree. I chose it as the display typeface for this site because like Frosty's old top hat, there's some magic in it which mysteriously whispers, "All I font for Christmas is you."`,
    website: 'https://www.lydiamariedesign.com/'
  },
  designers: [
    {
      image: 'https://lh3.googleusercontent.com/d57BD3LuAypRG-8IQf2xKbeIMYwr4PSup6AD8UKsSzt8-wYf5lpSWMw7VUw',
      name: 'TypeTogether',
      bio: `
        <p><a href="http://www.type-together.com">TypeTogether</a> is an independent type foundry committed to excellence in type design with a focus on editorial use. Known for the popular typefaces <a href="http://www.type-together.com/Adelle">Adelle</a> and <a href="http://www.type-together.com/Bree">Bree</a>, TypeTogether also creates custom type designs for corporate use, including their work for <a href="http://www.type-together.com/index.php?action=portal/viewContent&cntId_content=3301&id_section=141">Google Play Books</a>, <a href="http://www.type-together.com/index.php?action=portal/viewContent&cntId_content=3230&id_section=141">Clarín</a>, and <a href="http://www.type-together.com/index.php?action=portal/viewContent&cntId_content=3198&id_section=141">Apple</a>.</p>
        <p><a href="https://twitter.com/TypeTogether">Twitter</a></p>
      `
    }
  ],
  about: `
    <p>
      This friendly upright italic is the serif cousin of TypeTogether's award winning family <a href="http://www.type-together.com/Bree">Bree</a>.
      Designed by Veronika Burian and José Scaglione, Bree was originally released in 2008 and became an immediate success because of its originality, charming appearance and versatility.
    </p>
  `,
  chars: `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzž1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*`
})

module.exports = fonts
