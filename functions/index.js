const functions = require('firebase-functions')
const moment = require('moment')
const cors = require('cors')({origin:true})
const fonts = require('./fonts')
const {
  START_DATE,
  END_DATE,
  SERVE_ALL_FONTS,
  MAILGUN_API_KEY,
  MAILGUN_DOMAIN,
  EMAIL_RECIPIENT,
} = require('./env')
const mailgun = require('mailgun-js')({apiKey: MAILGUN_API_KEY, domain: MAILGUN_DOMAIN});

exports.fonts = functions.https.onRequest((req, res) => {
  const now = moment.utc()
  let { tz } = req.query
  if (!isNaN(tz) && Math.abs(tz) <= 720 && Math.abs(tz) > 0) {
    now.subtract(Number(tz) / 60, 'hours')
  }

  const isBefore = now < START_DATE
  const isAfter = now > END_DATE
  const isDuring = !isBefore && !isAfter

  let availableFonts = { total: 1 }
  if (SERVE_ALL_FONTS || isAfter) {
    availableFonts = fonts
    availableFonts.total = 31
  }
  else if (isDuring) {
    const date = Number(now.format('D'))
    availableFonts.total = date
    let i = 1
    while (i <= date) {
      availableFonts[i] = fonts[i]
      i++
    }
  }

  return cors(req, res, () => {
    const ONE_HOUR = 1 * 60 * 60
    const ONE_DAY = 24 * ONE_HOUR
    const ONE_YEAR = 365 * ONE_DAY
    const TEN_YEARS = 10 * ONE_YEAR
    res.set('Cache-Control', `public, max-age=${TEN_YEARS}, s-maxage=${TEN_YEARS}`)
    res.json(availableFonts)
  })
})

exports.time = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    res.json({ utc: moment.utc().format() })
  })
})

exports.contact = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    if (req.method !== 'POST') {
      return res.json(true)
    }

    const {
      name,
      email,
      message,
      typeface
    } = req.body

    if (!name || !name.length) {
      return res.status(400).json({
        message: 'A name is required'
      })
    }

    const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
    if (!emailRegex.test(email)) {
      return res.status(400).json({
        message: 'Email is invalid'
      })
    }

    if (!message || !message.length) {
      return res.status(400).json({
        message: 'A message is required'
      })
    }

    mailgun.messages().send({
      from: '"Xmas Visitor 🎄" <visitor@allifontforchristmas.com>',
      to: EMAIL_RECIPIENT,
      subject: 'Visitor Email',
      text: [
        `Name: ${name}`,
        `Email: ${email}`,
        `Message: ${message}`,
        `${typeface ? `Typeface: ${typeface}` : ''}`
      ].join('\n'),
    }, (err, body) => {
      if (err) {
        return res.status(400).json({
          message: 'Unable to send message',
          err
        })
      }
      res.json({
        message: 'Email sent!',
        body
      })
    })
  })
})

exports.env = functions.https.onRequest((req, res) => {
  if (!req.get('host').includes('localhost:5000')) {
    return res.status(400).json({
      message: 'Endpoint not found'
    })
  }
  return cors(req, res, () => {
    res.json(require('./env'))
  })
})
