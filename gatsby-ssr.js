import React from 'react'
import ReactDOMServer from 'react-dom/server'
import { ServerStyleSheets } from '@material-ui/core/styles'
import { default as minifyCss } from 'minify-css-string'
import Helmet from 'react-helmet'
import App from './src/components/App'

export const wrapRootElement = ({ element }) => {
  const sheets = new ServerStyleSheets()
  ReactDOMServer.renderToString(
    sheets.collect(
      <App>{element}</App>
    )
  )
  const css = minifyCss(sheets.toString())
  return (
    <App>
      <Helmet>
        <style>{css}</style>
      </Helmet>
      {element}
    </App>
  )
}
