import React from "react"
import Fonts from '../Fonts'
import { fontProps } from '../../../helpers/fonts'

const FontList = ({ fonts = [] }) => (
  <>
    <Fonts fonts={fonts} />
    {fonts.map((font, i) => (
      <h2 key={i} {...fontProps(font.name)}>{font.date}{font.name}</h2>
    ))}
  </>
)

export default FontList
