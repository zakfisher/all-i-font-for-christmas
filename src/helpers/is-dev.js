import isClient from './is-client'
export default isClient && window.location.href.includes('localhost')
