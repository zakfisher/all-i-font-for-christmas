import get from 'lodash.get'
import isClient from './is-client'
import cache from './cache'
export default () => {
  return isClient && (window.innerWidth < get(cache.get('theme'), 'breakpoints.values.md', 0))
}