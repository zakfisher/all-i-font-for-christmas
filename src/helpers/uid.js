export default (prefix = 'uid') => {
  return `${prefix}-${parseInt(Math.random() * 1000000)}`
}
